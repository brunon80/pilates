var gulp = require('gulp');
var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    rename = require('gulp-rename');
var tinylr;
var browserSync = require('browser-sync').create();
var uncss = require('gulp-uncss');

gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: ''
    },
  })
})

gulp.task('watch',['browserSync', 'styles'], function() {
  gulp.watch('sass/*.scss', ['styles']);
  gulp.watch('*.html', browserSync.reload);
  gulp.watch('views/*.html', browserSync.reload);
  gulp.watch('js/*.js', browserSync.reload);
});

gulp.task('styles', function() {
  return sass('sass', { style: 'expanded' })
    .pipe(gulp.dest('css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(uncss({
            html: ['index.html', 'views/*.html']
        }))
    .pipe(gulp.dest('css'))
    .pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task('default',['watch'], function () {
	
});