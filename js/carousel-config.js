//************** carousel & functions ****************

$(window).load(function() {

    var clicked = false;
// script para srollar a pagina
    function scrollNav() {
      $('.scroll').click(function(){  
        //console.log('cliquei');
        //Animate
        $('html, body').stop().animate({
            scrollTop: $( $(this).attr('href') ).offset().top - 160
        }, 400);
        return false;
      });
      $('.scrollTop a').scrollTop();
    }
    scrollNav();

// script pra mostar menus
    $('[data-toggle="tooltip"]').tooltip();

    $('#showmenu').click(function() {
        $('.drop-down-news-container').slideToggle("fast");

        if ($("#arrow-news").hasClass("icon-angle-down")) {

            $("#arrow-news").removeClass("icon-angle-down");
            $("#arrow-news").addClass("icon-angle-up");

        }else {
            
            $("#arrow-news").removeClass("icon-angle-up");
            $("#arrow-news").addClass("icon-angle-down");

        }
            
           
        
        

        
    });

// scripts do carrossel
    $('.owl-carousel').owlCarousel({
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        loop: true,
        margin: 10,
        nav: false,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    });

    $('.owl-carousel-depoiment').owlCarousel({
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        loop: true,
        margin: 10,
        nav: false,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 2
            }
        }
    });


            

    $('.owl-carousel-unity').owlCarousel({
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        loop: true,
        margin: 10,
        nav: true,
        navText: ["<span class='icon-angle-left'></span>","<span class='icon-angle-right'></span>"],
        dots: true,
        items:1,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });

});
