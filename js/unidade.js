var pilatesApp = angular.module('instituto-pilates', []);
    pilatesApp.controller('clinicCtl', function($scope, $http) {

        //*************** AgularJS functions and variables *****************

        $scope.clinicsArray = [];
        $scope.uniqueClinicsArray = [];
        $scope.stateGroup = [];
        $scope.stateSelcted = 'BRASIL'
        var url = "https://www.instpilates.com.br/?json=get_posts&post_type=clinica&custom_fields=_pdt_estado,_pdt_cidade";

        //*************** Requisição dos estados *************


        $http({
            url: url,
            headers: {
                'Content-Type': "application/json"
            }
        }).then(function successCallback(data) {
            $scope.clinics = data.data.posts;
            $scope.clinicNames($scope.clinics);

        }, function errorCallback(response) {
            console.log(response);

        });
        //*************** Fim da requisição dos estados *************

        $scope.clinicNames = function(clinics) {

            for (var i = 0; i < clinics.length; i++) {


                $scope.clinicsArray.push(clinics[i].custom_fields._pdt_estado[0]);

            }

            $.each($scope.clinicsArray, function(i, el) {
                if ($.inArray(el, $scope.uniqueClinicsArray) === -1) $scope.uniqueClinicsArray.push(el);
            });
            console.log($scope.clinics);
        }


        $scope.clinicSelected = function() {


            window.location.href = "/views/lista-de-unidades.html?e=" + $scope.stateSelcted;



        }

    });