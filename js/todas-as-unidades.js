    var pilatesApp = angular.module('instituto-pilates', []);

    pilatesApp.directive('once', function(){
          return {
            restrict: "A",
            template: '<div>{{ myDirective }}</div>', // where myDirective binds to scope.myDirective
            scope: {
              oneUnity: '='
            },
            link: function(scope, element, attrs) {
              console.log('Do action with data', scope.oneUnity);
            }
          };
        });

    pilatesApp.controller('clinicCtl', function($scope, $http) {
        $("body").tooltip({
            selector: '[data-toggle=tooltip]'
        });
        //*************** AgularJS functions and variables *****************





        $scope.clinicsArray = [];
        $scope.uniqueClinicsArray = [];
        $scope.stateGroup = [];
        $scope.stateSelcted = 'BRASIL'
        var url = "https://www.instpilates.com.br/?json=get_posts&post_type=clinica&custom_fields=_pdt_estado,_pdt_cidade";

        //*************** Requisição dos estados ***************

        $scope.loading = true;
        $http({
            url: url,
            headers: {
                'Content-Type': "application/json"
            }
        }).then(function successCallback(data) {
            $scope.clinics = data.data.posts;
            $scope.clinicNames($scope.clinics);
            $scope.criaListaUnidades($scope.clinics); 
            //$scope.clinicUnitys($scope.uniqueClinicsArray);
            $scope.stateSelcted = $scope.getQueryVariable("e");
            $scope.clinicSelected();
            $scope.loading = false;
            console.log(data);
        }, function errorCallback(response) {
            console.log(response);

        });
        //*************** Fim da requisição dos estados *************



        $scope.listaUnidades = [];

         $scope.existeObjetoComNome = function(nome) {
            $scope.objetoAchado = $scope.listaUnidades.filter(function(objeto) {return objeto.nome == nome});
            return $scope.objetoAchado;
        }

        $scope.criaListaUnidades =  function(clinics){

            //console.log($scope.clinics);

            for (var i = 0; i < clinics.length; i++) {

                $scope.objeto = clinics[i];
                $scope.objetoExistente = $scope.existeObjetoComNome($scope.objeto.custom_fields._pdt_estado[0]);
                if ($scope.objetoExistente.length != 0) {
                    for (var j = 0; j < $scope.listaUnidades.length; j++) {
                        if ($scope.objetoExistente[0].nome == $scope.listaUnidades[j].nome) {
                            $scope.listaUnidades[j].lugares.push(
                                {
                                    cidade: $scope.objeto.custom_fields._pdt_cidade[0],
                                    bairro: $scope.objeto.title,
                                    url: $scope.objeto.url
                                }
                            );
                        }
                    }      
                } else {
                    $scope.listaUnidades.push(
                        {
                            nome: $scope.objeto.custom_fields._pdt_estado[0],                             
                            lugares: [
                                {
                                    cidade: $scope.objeto.custom_fields._pdt_cidade[0], 
                                    bairro: $scope.objeto.title, 
                                    url: $scope.objeto.url
                                } 
                            ]
                        });
                }
                
            }
            console.log($scope.listaUnidades);
        }

        

        $scope.clinicNames = function(clinics) {

            for (var i = 0; i < clinics.length; i++) {


                $scope.clinicsArray.push(clinics[i].custom_fields._pdt_estado[0]);

            }

            $.each($scope.clinicsArray, function(i, el) {
                if ($.inArray(el, $scope.uniqueClinicsArray) === -1) $scope.uniqueClinicsArray.push(el);
            });
            //console.log($scope.uniqueClinicsArray);
        }

        $scope.getQueryVariable = function(variable) {
            var query = window.location.search.substring(1);
            var vars = query.split('&');
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split('=');
                if (decodeURIComponent(pair[0]) == variable) {
                    return decodeURIComponent(pair[1]);
                }
            }
            return variable;
            //console.log('Query variable %s not found', variable);
        }

        
        $scope.clinicSelected = function() {


            //window.location.href = "/views/lista-de-unidades.html?e=" + $scope.stateSelcted;
            //window.location = url.replace("/views/lista-de-unidades.html?e=" + $scope.stateSelcted);
            if ($scope.stateSelcted != "e") {
                //console.log("eh diferente POHA");
                var newurl = "/views/lista-de-unidades.html?e=" + $scope.stateSelcted;
                window.location.href = newurl;
            }
            

            

            $scope.stateGroup = [];
            for (var i = 0; i < $scope.clinics.length; i++) {

                if (decodeURIComponent($scope.clinics[i].custom_fields._pdt_estado[0]) == $scope.stateSelcted) {
                    $scope.stateGroup.push($scope.clinics[i]);
                }
            }

            

        }

        $scope.clinicUnitys = function(estado) {




            $scope.clinicUnitysState = [];
            for (var i = 0; i < $scope.clinics.length; i++) {

                if (decodeURIComponent($scope.clinics[i].custom_fields._pdt_estado[0]) == estado) {
                    $scope.clinicUnitysState.push($scope.clinics[i]);
                    
                }
            }



        }

        // zona de funcoes de teste

        var counter1 = 0;
        var counter2 = 3;
        $scope.resetVariables = function() {

            counter1 = 0;
            counter2 = 3;
            console.log("entrei");
        }


        $scope.detectFirstAndLast = function(index) {
            //console.log($( window ).width());
            if ($(window).width() > 768) {

                if (parseInt(index) % 3 == 0) {

                    setTimeout(function() {
                        //console.log("entrei");
                        $('.item-custon').eq(counter1).addClass('no-margin-left');
                        counter1 = counter1 + 4;

                    }, 1);

                }

                if (parseInt(index) % 2 == 1) {
                    setTimeout(function() {
                        $('.item-custon').eq(counter2).addClass('no-margin-right');
                        counter2 = counter2 + 4;

                    }, 1);

                }
            }
            $scope.resetVariables();

        }




    });


