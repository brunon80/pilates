var pilatesApp = angular.module('instituto-pilates', []);
    pilatesApp.controller('clinicCtl', function($scope, $http) {


        //***** Jquery ********

        $("#first-square").click(function() {

            $(".select-option").addClass("start");
            $(".select-option").removeClass("middle");
            $(".select-option").removeClass("end");

            $("#first-form").addClass("will-appear");
            setTimeout(function() {
                $("#first-form").addClass("custon-fade");
            }, 200);
            $("#first-form").removeClass("will-disappear");


            $("#second-form").addClass("will-disappear");
            $("#second-form").removeClass("custon-fade");
            $("#third-form").addClass("will-disappear");
            $("#third-form").removeClass("custon-fade");

        });


        $("#second-square").click(function() {

            $(".select-option").addClass("middle");
            $(".select-option").removeClass("start");
            $(".select-option").removeClass("end");

            $("#second-form").addClass("will-appear");
            setTimeout(function() {
                $("#second-form").addClass("custon-fade");
            }, 200);
            $("#second-form").removeClass("will-disappear");

            $("#first-form").addClass("will-disappear");
            $("#first-form").removeClass("custon-fade");
            $("#third-form").addClass("will-disappear");
            $("#third-form").removeClass("custon-fade");

        });

        $("#third-square").click(function() {

            $(".select-option").addClass("end");
            $(".select-option").removeClass("start");
            $(".select-option").removeClass("middle");

            $("#third-form").addClass("will-appear");
            setTimeout(function() {
                $("#third-form").addClass("custon-fade");
            }, 200);
            $("#third-form").removeClass("will-disappear");


            $("#first-form").addClass("will-disappear");
            $("#first-form").removeClass("custon-fade");
            $("#second-form").addClass("will-disappear");
            $("#second-form").removeClass("custon-fade");

        });


        //*************** AgularJS functions and variables *****************

        $scope.clinicsArray = [];
        $scope.uniqueClinicsArray = [];
        $scope.stateGroup = [];
        $scope.stateSelcted = 'BRASIL'
        var url = "https://www.instpilates.com.br/?json=get_posts&post_type=clinica&custom_fields=_pdt_estado,_pdt_cidade";

        //*************** Requisição dos estados *************


        $http({
            url: url,
            headers: {
                'Content-Type': "application/json"
            }
        }).then(function successCallback(data) {
            $scope.clinics = data.data.posts;
            $scope.clinicNames($scope.clinics);

        }, function errorCallback(response) {
            console.log(response);

        });
        //*************** Fim da requisição dos estados *************

        $scope.clinicNames = function(clinics) {

            for (var i = 0; i < clinics.length; i++) {


                $scope.clinicsArray.push(clinics[i].custom_fields._pdt_estado[0]);

            }

            $.each($scope.clinicsArray, function(i, el) {
                if ($.inArray(el, $scope.uniqueClinicsArray) === -1) $scope.uniqueClinicsArray.push(el);
            });
            console.log($scope.clinics);
        }


        $scope.clinicSelected = function() {


            window.location.href = "/views/lista-de-unidades.html?e=" + $scope.stateSelcted;



        }

    });